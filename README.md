# Gousto – Frontend Test

## About

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

I decided to use this as it was a fast way to have all the tools I needed so I can only focus on solving the problem.

I like that it works out of the box and it's very easy to just start writing code, but if I was to start this project from scratch for production, I would spend more time in the structure for components. For now I just followed the conventions of create-react-app.

I am not fully convinced on my decision to name files `index` inside each component, as it created confusion at times. I would probably give them more meaningful names if I was to redo this.


## Install

Run `yarn` (`yarn` came by default with create react app. I don't personaly thing it is needed for this project and the project should still work with `npm install`).


## Run the project

* `npm start` will run the app in development mode ([http://localhost:3000](http://localhost:3000))
* `npm test` will run the tests


## Project structure

`create-react-app` comes with a predefined structure which I followed as it worked well for what I needed to do.

All the changes I've made are inside `src`.

## Approach

I took a TDD approach to the test.

You should be able to see the evolution of the codebase by going commit by commit.

I started from the inside out, trying to build the smallest blocks first and to use the "just components" approach.

As I don't have a lot of experience with Redux (this is something that I want to improve in the near future) and this project was simple from a data point of view, I went to use the Context API for managing data. I've ended up using axios for fetching the data from the API due to it's `cancel` feature.

In order to satisfy the `back`/`forward` requirement I've went with using `react-router-dom` and having sub-pages for each category.


## Closing thoughts

This was a very interesting test (it's almost a full app in itself) and, although it took me quite some time to finish it, I enjoyed doing it. There are multiple things I'd improve (looking at how I use the data, introducing Redux, not relying on URL to set the active category, etc) but I've tried keeping it as clean and as simple as possible.

Thank you for taking the time to review it.
