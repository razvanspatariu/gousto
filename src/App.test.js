import React from 'react';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { mount } from 'enzyme';
import { MemoryRouter } from 'react-router-dom';
import App from './App';
import Categories from './components/Categories';
import Products from './components/Products';
import { DUMMY_API_CATEGORIES, DUMMY_API_PRODUCTS } from './dummyData';
import { API_CATEGORIES, API_PRODUCTS, TEXT_ERROR_NO_CATEGORIES, TEXT_ERROR_NO_PRODUCTS_IN_CATEGORY } from './config';

describe('App', () => {
  const mock = new MockAdapter(axios);
  const flushPromises = () => new Promise(resolve => setImmediate(resolve));

  describe('when the user visits the first page', () => {
    let wrapper;

    beforeEach(() => {
      wrapper = mount(
        <MemoryRouter initialEntries={['/']}>
          <App />
        </MemoryRouter>,
      );
    });

    it('renders as expected', () => {
      expect(wrapper.html()).toMatchSnapshot();
    });

    it('renders the Categories component', () => {
      expect(wrapper.find(Categories).exists()).toBe(true);
    });

    it('does not render the Products component', () => {
      expect(wrapper.find(Products).exists()).toBe(false);
    });
  });

  describe('when the user visits a categories page', () => {
    let wrapper;

    beforeEach(() => {
      wrapper = mount(
        <MemoryRouter initialEntries={[`/${DUMMY_API_CATEGORIES}`]}>
          <App />
        </MemoryRouter>,
      );
    });

    it('renders as expected', () => {
      expect(wrapper.html()).toMatchSnapshot();
    });

    it('renders the Categories component', () => {
      expect(wrapper.find(Categories).exists()).toBe(true);
    });

    it('renders the Products component', () => {
      expect(wrapper.find(Products).exists()).toBe(true);
    });
  });

  describe('when the App loads', () => {
    let wrapper;
    let app;

    beforeEach(() => {
      wrapper = mount(
        <MemoryRouter initialEntries={[`/${DUMMY_API_CATEGORIES}`]}>
          <App />
        </MemoryRouter>,
      );

      app = wrapper.find(App);
    });

    it('passes the consumer data to Categories', () => {
      const categoriesProps = wrapper.find(Categories).props();
      const appState = app.state().categories;

      expect(categoriesProps.categories).toEqual(appState.categories);
      expect(categoriesProps.error).toEqual(appState.error);
      expect(categoriesProps.isLoading).toEqual(appState.isLoading);
    });

    it('sets the categories state to loading', () => {
      expect(app.state().categories).toEqual({
        categories: [],
        error: TEXT_ERROR_NO_CATEGORIES,
        isLoading: true,
      });
    });

    it('sets the products state to loading', () => {
      expect(app.state().products).toEqual({
        products: [],
        error: TEXT_ERROR_NO_PRODUCTS_IN_CATEGORY,
        isLoading: true,
      });
    });

    describe('and the categories API does not return the expected results', () => {
      beforeEach(() => {
        mock.onGet(API_CATEGORIES).reply(404);

        wrapper = mount(
          <MemoryRouter initialEntries={[`/${DUMMY_API_CATEGORIES}`]}>
            <App />
          </MemoryRouter>,
        );

        app = wrapper.find(App);
      });

      it('sets the state to show an error', async () => {
        await flushPromises();
        wrapper.update();

        expect(app.state().categories).toEqual({
          categories: [],
          error: TEXT_ERROR_NO_CATEGORIES,
          isLoading: false,
        });
      });
    });

    describe('and the categories API returns the expected results', () => {
      beforeEach(() => {
        mock.onGet(API_CATEGORIES).reply(200, {
          data: DUMMY_API_CATEGORIES
        });

        wrapper = mount(
          <MemoryRouter initialEntries={[`/${DUMMY_API_CATEGORIES}`]}>
            <App />
          </MemoryRouter>,
        );

        app = wrapper.find(App);
      });

      it('sets the state to show the categories', async () => {
        await flushPromises();
        wrapper.update();

        expect(app.state().categories).toEqual({
          categories: DUMMY_API_CATEGORIES,
          error: TEXT_ERROR_NO_CATEGORIES,
          isLoading: false,
        });
      });
    });

    describe('and the products API does not return the expected results', () => {
      beforeEach(() => {
        mock.onGet(API_PRODUCTS).reply(404);

        wrapper = mount(
          <MemoryRouter initialEntries={[`/${DUMMY_API_CATEGORIES}`]}>
            <App />
          </MemoryRouter>,
        );

        app = wrapper.find(App);
      });

      it('sets the state to show an error', async () => {
        await flushPromises();
        wrapper.update();

        expect(app.state().products).toEqual({
          products: [],
          error: TEXT_ERROR_NO_PRODUCTS_IN_CATEGORY,
          isLoading: false,
        });
      });
    });

    describe('and the products API returns the expected results', () => {
      beforeEach(() => {
        mock.onGet(API_PRODUCTS).reply(200, {
          data: DUMMY_API_PRODUCTS
        });

        wrapper = mount(
          <MemoryRouter initialEntries={[`/${DUMMY_API_CATEGORIES}`]}>
            <App />
          </MemoryRouter>,
        );

        app = wrapper.find(App);
      });

      it('sets the state to show the products', async () => {
        await flushPromises();
        wrapper.update();

        expect(app.state().products).toEqual({
          products: DUMMY_API_PRODUCTS,
          error: TEXT_ERROR_NO_PRODUCTS_IN_CATEGORY,
          isLoading: false,
        });
      });
    });
  });
});
