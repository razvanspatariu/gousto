import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import axios from 'axios';
import './App.css';
import Categories from './components/Categories';
import CategoryProducts from './components/CategoryProducts';
import { API_CATEGORIES, API_PRODUCTS, TEXT_ERROR_NO_CATEGORIES, TEXT_ERROR_NO_PRODUCTS_IN_CATEGORY } from './config';
import { CategoriesContext } from './contexts/categories';
import { ProductsContext } from './contexts/products';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      categories: {
        categories: [],
        error: TEXT_ERROR_NO_CATEGORIES,
        isLoading: true,
      },
      products: {
        products: [],
        error: TEXT_ERROR_NO_PRODUCTS_IN_CATEGORY,
        isLoading: true,
      },
    };
  }

  getData = (api, cancelToken) => {
    return axios
      .get(api, { cancelToken })
      .then((response) => {
        if (!response.data.status === 'ok') {
          throw response.data;
        }

        return response.data;
      })
      .then((response) => {
        return response.data;
      })
      .catch(() => {
        return [];
    });
  }

  async getCategories() {
    if(!this.state.categories.categories.length) {
      const categories = await this.getData(API_CATEGORIES, this._asyncRequestCategories.token);

      this.setState({
        categories: {
          categories,
          error: TEXT_ERROR_NO_CATEGORIES,
          isLoading: false,
        },
      });
    }
  }

  async getProducts() {
    if(!this.state.products.products.length) {
      const products = await this.getData(API_PRODUCTS, this._asyncRequestProducts.token);

      this.setState({
        products: {
          products,
          error: TEXT_ERROR_NO_PRODUCTS_IN_CATEGORY,
          isLoading: false,
        },
      });
    }
  }

  componentDidMount() {
    this._asyncRequestCategories = axios.CancelToken.source();
    this._asyncRequestProducts = axios.CancelToken.source();

    this.getCategories();
    this.getProducts();
  }

  componentWillUnmount() {
    if(this._asyncRequestCategories) {
      this._asyncRequestCategories.cancel();
    }

    if(this._asyncRequestProducts) {
      this._asyncRequestProducts.cancel();
    }
  }

  render() {
    return (
      <div className="app">
        <Router>
          <CategoriesContext.Provider value={this.state.categories}>
            <ProductsContext.Provider value={this.state.products}>
              <CategoriesContext.Consumer>
                {(contextProps) => (
                  <Route path="/:categoryID?" render={(props) => <Categories {...contextProps} {...props} />} />
                )}
              </CategoriesContext.Consumer>

              <Route path="/:categoryID" render={(props) => <CategoryProducts {...props} />} />
            </ProductsContext.Provider>
          </CategoriesContext.Provider>
        </Router>
      </div>
    );
  }
}

export default App;
