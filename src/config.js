const API_CATEGORIES = 'https://api.gousto.co.uk/products/v2.0/categories';
const API_PRODUCTS = 'https://api.gousto.co.uk/products/v2.0/products?includes%5B%5D=categories&includes%5B%5D=attributes&sort=position&image_sizes%5B%5D=365&i';

const CLASS_IS_ACTIVE = 'is-active';

const TEXT_ERROR_GENERIC = 'Something went wrong.';
const TEXT_ERROR_NO_CATEGORIES = 'There was an issue fetching categories.';
const TEXT_ERROR_NO_PRODUCTS = 'No products found. Please try a different search term.';
const TEXT_ERROR_NO_PRODUCTS_IN_CATEGORY = 'No products found within this category. Please try a different one.';

export {
  API_CATEGORIES,
  API_PRODUCTS,
  CLASS_IS_ACTIVE,
  TEXT_ERROR_GENERIC,
  TEXT_ERROR_NO_CATEGORIES,
  TEXT_ERROR_NO_PRODUCTS,
  TEXT_ERROR_NO_PRODUCTS_IN_CATEGORY,
};
