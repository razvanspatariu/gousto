import React from 'react';
import { TEXT_ERROR_NO_PRODUCTS_IN_CATEGORY } from '../config';

export const ProductsContext = React.createContext({
  products: [],
  error: TEXT_ERROR_NO_PRODUCTS_IN_CATEGORY,
  isLoading: true,
});
