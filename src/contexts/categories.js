import React from 'react';
import { TEXT_ERROR_NO_CATEGORIES } from '../config';

export const CategoriesContext = React.createContext({
  categories: [],
  error: TEXT_ERROR_NO_CATEGORIES,
  isLoading: true,
});
