import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

const rrd = require('react-router-dom');

rrd.BrowserRouter = ({ children }) => <Fragment>{children}</Fragment>;

rrd.Redirect = () => <Fragment />;

rrd.BrowserRouter.propTypes = {
  children: PropTypes.node.isRequired,
};

module.exports = rrd;
