import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Link } from 'react-router-dom';
import './index.css';
import Error from '../Error';
import Loading from '../Loading';
import { CLASS_IS_ACTIVE, TEXT_ERROR_GENERIC } from '../../config';

const renderCategory = (category, activeCategoryID) => {
  const classes = classNames(
    'categories__item',
    {
      [CLASS_IS_ACTIVE]: category.id === activeCategoryID,
    },
  );
  return (<Link to={`/${category.id}`} key={category.id} className={classes}>{category.title}</Link>);
}

const Categories = ({ categories, error, isLoading, match }) => {
  if(categories.length) return (
    <nav className="categories">
      {categories.map(category => renderCategory(category, match.params.categoryID))}
    </nav>
  );

  if(isLoading) return <Loading/>;

  return <Error>{error || TEXT_ERROR_GENERIC}</Error>;
};

Categories.defaultProps = {
  categories: [],
  error: null,
  isLoading: false,
  match: {
    params: {
      categoryID: null,
    },
  },
};

Categories.propTypes = {
  categories: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
  })),
  error: PropTypes.string,
  isLoading: PropTypes.bool,
  match: PropTypes.shape({
    params: PropTypes.shape({
      categoryID: PropTypes.string,
    }),
  }),
};

export default Categories;
