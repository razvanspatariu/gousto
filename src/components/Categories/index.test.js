import React, { cloneElement } from 'react';
import ReactDOM from 'react-dom';
import { mount } from 'enzyme';
import { Link, MemoryRouter } from 'react-router-dom';
import Categories from './';
import Error from '../Error';
import { CLASS_IS_ACTIVE, TEXT_ERROR_GENERIC } from '../../config';
import { DUMMY_API_CATEGORIES } from '../../dummyData';

describe('Categories', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(<MemoryRouter><Categories /></MemoryRouter>);
  });

  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<MemoryRouter><Categories isLoading /></MemoryRouter>, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  describe('when no props are passed', () => {
    it('shows an error', () => {
      expect(wrapper.find(Error).exists()).toBe(true);
    });

    it('shows a generic error message', () => {
      expect(wrapper.find(Error).text()).toBe(TEXT_ERROR_GENERIC);
    });
  });

  describe('when a custom error message is passed', () => {
    const CUSTOM_ERROR_MESSAGE = 'This is a custom error message';

    beforeEach(() => {
      wrapper.setProps({
        children: cloneElement(wrapper.props().children,
          { error: CUSTOM_ERROR_MESSAGE }
        ),
      });
    });

    it('shows the custom error message', () => {
      expect(wrapper.find(Error).text()).toBe(CUSTOM_ERROR_MESSAGE);
    });

    it('does not show the generic error message', () => {
      expect(wrapper.find(Error).text()).not.toBe(TEXT_ERROR_GENERIC);
    });
  });

  describe('when the isLoading prop is passed', () => {
    beforeEach(() => {
      wrapper.setProps({
        children: cloneElement(wrapper.props().children,
          { isLoading: true }
        ),
      });
    });

    it('renders the loading text on the page', () => {
      expect(wrapper.text()).toContain('Loading');
    });
  });

  describe('when the categories prop is passed', () => {
    beforeEach(() => {
      wrapper.setProps({
        children: cloneElement(wrapper.props().children,
          { categories: DUMMY_API_CATEGORIES }
        ),
      });
    });

    it('renders the categories on the page', () => {
      expect(wrapper.find(Link).exists()).toBe(true);

      wrapper.find(Link).forEach((item, index) => {
        expect(wrapper.find(Link).at(index).text()).toEqual(DUMMY_API_CATEGORIES[index].title);
      });
    });

    it('creates a link with the ID for each item', () => {
      wrapper.find(Link).forEach((item, index) => {
        expect(item.props().to).toBe(`/${DUMMY_API_CATEGORIES[index].id}`);
      });
    });

    describe('and the user is on the main page', () => {
      it('does not set any items as active', () => {
        expect(wrapper.find(`.categories__item.${CLASS_IS_ACTIVE}`).exists()).toBe(false);
      });
    });

    describe('and the user is on a category page', () => {
      const RANDOM_CATEGORY_INDEX = Math.floor(Math.random() * DUMMY_API_CATEGORIES.length);

      const RANDOM_CATEGORY = DUMMY_API_CATEGORIES[RANDOM_CATEGORY_INDEX];

      beforeEach(() => {
        wrapper.setProps({
          match: {
            params: {
              categoryID: RANDOM_CATEGORY.id,
            },
          },
        });
      });

      it('sets that category item as active', () => {
        const categoryItem = wrapper.find(Link).at(RANDOM_CATEGORY_INDEX);

        expect(categoryItem.exists()).toBe(true);

        expect(categoryItem.text()).toBe(RANDOM_CATEGORY.title);
      });
    });
  });
});
