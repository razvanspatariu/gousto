import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import Product from './';
import { CLASS_IS_ACTIVE } from '../../config';
import { DUMMY_API_PRODUCTS } from '../../dummyData';

describe('Product', () => {
  let wrapper;
  const RANDOM_PRODUCT = DUMMY_API_PRODUCTS[Math.floor(Math.random() * DUMMY_API_PRODUCTS.length)];

  beforeEach(() => {
    wrapper = shallow(<Product {...RANDOM_PRODUCT} />);
  });

  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Product {...RANDOM_PRODUCT} />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('renders the title', () => {
    const title = wrapper.find('.product__title');

    expect(title.exists()).toBe(true);
    expect(title.text()).toContain(RANDOM_PRODUCT.title);
  });

  it('does not render the description', () => {
    const description = wrapper.find('product__description');

    expect(description.exists()).toBe(false);
  });

  describe('when the user clicks on the product', () => {
    beforeEach(() => {
      wrapper.simulate('click');
    });

    it('adds an active class on the title element', () => {
      expect(wrapper.find('.product__title').hasClass(CLASS_IS_ACTIVE)).toBe(true);
    });

    it('shows the description', () => {
      const description = wrapper.find('.product__description');

      expect(description.exists()).toBe(true);
      expect(description.text()).toContain(RANDOM_PRODUCT.description);
    });

    describe('and the user clicks again on the product', () => {
      beforeEach(() => {
        wrapper.simulate('click');
      });

      it('removes the active class from the title element', () => {
        expect(wrapper.find('.product__title').hasClass(CLASS_IS_ACTIVE)).toBe(false);
      });

      it('hides the description', () => {
        expect(wrapper.find('.product__description').exists()).toBe(false);
      });
    });
  });
});
