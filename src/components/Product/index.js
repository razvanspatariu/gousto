import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import './index.css';
import { CLASS_IS_ACTIVE } from '../../config';

export default class Product extends Component {
  constructor(props) {
    super(props);

    this.state = { isActive: false };
  }

  handleClick = () => {
    this.setState({ isActive: !this.state.isActive });
  }

  render() {
    const { description, title } = this.props;
    const { isActive } = this.state;

    const classesTitle = classNames(
      'product__title',
      {
        [CLASS_IS_ACTIVE]: isActive,
      },
    );

    return (
      <li className="product" onClick={this.handleClick}>
        <span className={classesTitle}>{title}</span>
        {
          isActive && <div className="product__description">{description}</div>
        }
      </li>
    );
  }
}

Product.propTypes = {
  description: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
};
