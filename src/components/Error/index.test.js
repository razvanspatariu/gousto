import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import Error from './';

describe('Error', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Error>Something went wrong</Error>, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('renders as expected', () => {
    expect(shallow(<Error>Something went wrong</Error>).html()).toMatchSnapshot();
  });
});
