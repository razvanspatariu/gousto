import React from 'react';
import PropTypes from 'prop-types';
import './index.css';

const Error = ({ children }) => <div className="error">{children}</div>;

Error.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Error;
