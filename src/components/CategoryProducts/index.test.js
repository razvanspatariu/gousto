import React from 'react';
import ReactDOM from 'react-dom';
import { mount } from 'enzyme';
import CategoryProducts from './';
import Error from '../Error';
import Products from '../Products';
import { TEXT_ERROR_NO_PRODUCTS_IN_CATEGORY } from '../../config';
import { DUMMY_API_CATEGORIES, DUMMY_API_PRODUCTS } from '../../dummyData';
import { ProductsContext } from '../../contexts/products';

describe('CategoryProducts', () => {
  let wrapper;
  const CATEGORY_WITH_PRODUCTS = DUMMY_API_CATEGORIES[0];
  const CATEGORY_WITHOUT_PRODUCTS = DUMMY_API_CATEGORIES[2];

  const props = {
    match: {
      params: {
        categoryID: CATEGORY_WITH_PRODUCTS.id,
      },
    },
  };

  const context = {
    error: TEXT_ERROR_NO_PRODUCTS_IN_CATEGORY,
    isLoading: false,
    products: DUMMY_API_PRODUCTS,
  };

  beforeEach(() => {
    wrapper = mount(
      <ProductsContext.Provider value={context}>
        <CategoryProducts {...props} />
      </ProductsContext.Provider>
    );
  });

  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<ProductsContext.Provider value={context}><CategoryProducts {...props} /></ProductsContext.Provider>, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('renders the Products component', () => {
    expect(wrapper.find(Products).exists()).toBe(true);
  });

  describe('when the user is on a page that has products matching the category', () => {
    it('sends the products as props to Products component', () => {
      expect(wrapper.find(Products).props().products).toEqual([ DUMMY_API_PRODUCTS[0], DUMMY_API_PRODUCTS[2]]);
    });
  });

  describe('when the user is on a page that has no products matching the category', () => {
    beforeEach(() => {
      const propsWithoutProducts = {
        match: {
          params: {
            categoryID: CATEGORY_WITHOUT_PRODUCTS.id,
          },
        },
      };

      wrapper = mount(
        <ProductsContext.Provider value={context}>
          <CategoryProducts {...propsWithoutProducts} />
        </ProductsContext.Provider>
      );
    });

    it('displays a custom error message', () => {
      expect(wrapper.find(Error).text()).toContain(TEXT_ERROR_NO_PRODUCTS_IN_CATEGORY);
    });
  });
});
