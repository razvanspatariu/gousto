import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Products from '../Products';
import './index.css';
import { ProductsContext } from '../../contexts/products';

export default class CategoryProducts extends Component {
  static contextType = ProductsContext;

  constructor(props) {
    super(props);

    this.state = {};
  }

  filterResults = categoryID => this.context.products.filter(product => {
    let hasCategory = false;

    product.categories.forEach(category => {
      if(category.id === categoryID) {
        hasCategory = true;
      }
    });

    return hasCategory;
  });

  render() {
    const { match } = this.props;

    return <Products error={this.context.error} isLoading={this.context.isLoading} products={this.filterResults(match.params.categoryID)} />;
  }
}

CategoryProducts.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      categoryID: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};
