import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './index.css';
import Error from '../Error';
import Loading from '../Loading';
import Product from '../Product';
import { TEXT_ERROR_GENERIC, TEXT_ERROR_NO_PRODUCTS } from '../../config';

export default class Products extends Component {
  constructor(props) {
    super(props);

    this.state = {
      searchValue: '',
    };
  }

  handleChange = (event) => {
    const { value } = event.target;

    this.setState({
      searchValue: value,
    });
  }

  filterProducts = (products, searchValue) => products.filter(product =>
    product.title.toLowerCase().includes(searchValue) ||
    product.description.toLowerCase().includes(searchValue)
  );

  renderErrorNoProducts = (error) => <Error>{error}</Error>

  renderProducts = (products) => (
    <ul className="products__list">
      {products.map(product => <Product key={product.id} {...product} />)}
    </ul>
  );

  renderSearchResults = (products, searchValue) => {
    const results = this.filterProducts(products, searchValue.toLowerCase());

    return results.length ? this.renderProducts(results) : this.renderErrorNoProducts(TEXT_ERROR_NO_PRODUCTS);
  }

  render() {
    const { products, error, isLoading } = this.props;
    const { searchValue } = this.state;

    if(products.length) return (
      <div className="products">
        <input
          className="products__search"
          type="search"
          value={searchValue}
          onChange={
            event => this.handleChange(event)
          }
        />
        {
          !!searchValue ?
            this.renderSearchResults(products, searchValue) :
            this.renderProducts(products)
        }
      </div>
    );

    if(isLoading) return <Loading/>;

    return <Error>{error || TEXT_ERROR_GENERIC}</Error>;
  }
}

Products.defaultProps = {
  products: [],
  error: null,
  isLoading: false,
}

Products.propTypes = {
  products: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    categories: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
    })),
  })),
  error: PropTypes.string,
  isLoading: PropTypes.bool,
};
