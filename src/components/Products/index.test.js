import React from 'react';
import ReactDOM from 'react-dom';
import { mount } from 'enzyme';
import Product from '../Product';
import Products from './';
import Error from '../Error';
import { TEXT_ERROR_GENERIC, TEXT_ERROR_NO_PRODUCTS } from '../../config';
import { DUMMY_API_PRODUCTS } from '../../dummyData';

describe('Products', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(<Products />);
  });

  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Products />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  describe('when no props are passed', () => {
    it('shows an error', () => {
      expect(wrapper.find(Error).exists()).toBe(true);
    });

    it('shows a generic error message', () => {
      expect(wrapper.find(Error).text()).toBe(TEXT_ERROR_GENERIC);
    });
  });

  describe('when a custom error message is passed', () => {
    const CUSTOM_ERROR_MESSAGE = 'This is a custom error message';

    beforeEach(() => {
      wrapper.setProps({ error: CUSTOM_ERROR_MESSAGE });
    });

    it('shows the custom error message', () => {
      expect(wrapper.find(Error).text()).toBe(CUSTOM_ERROR_MESSAGE);
    });

    it('does not show the generic error message', () => {
      expect(wrapper.find(Error).text()).not.toBe(TEXT_ERROR_GENERIC);
    });
  });

  describe('when the isLoading prop is passed', () => {
    beforeEach(() => {
      wrapper.setProps({ isLoading: true });
    });

    it('renders the loading text on the page', () => {
      expect(wrapper.text()).toContain('Loading');
    });
  });

  describe('when the products prop is passed', () => {
    beforeEach(() => {
      wrapper.setProps({ products: DUMMY_API_PRODUCTS });
    });

    it('renders a search input on the page', () => {
      expect(wrapper.find('.products__search').exists()).toBe(true);
    });

    it('renders the products on the page', () => {
      expect(wrapper.find(Product)).toHaveLength(DUMMY_API_PRODUCTS.length);
    });

    describe('and the user inputs a value in the string', () => {
      describe('and that value is not part of the title or description of any products', () => {
        beforeEach(() => {
          wrapper.find('.products__search').simulate('change', { target: { value: 'xyz' } });
        });

        it('does not display any products', () => {
          expect(wrapper.find(Product)).toHaveLength(0);
        });

        it('shows an error', () => {
          expect(wrapper.find(Error).exists()).toBe(true);
        });

        it('shows an error message regarding no products found', () => {
          expect(wrapper.find(Error).text()).toBe(TEXT_ERROR_NO_PRODUCTS);
        });
      });

      describe('and the value is part of the title of some products', () => {
        const SEARCH_TERM = 'Search Title';

        beforeEach(() => {
          wrapper.find('.products__search').simulate('change', { target: { value: SEARCH_TERM } });
        });

        it('only displays the products that match the title', () => {
          expect(wrapper.find(Product)).toHaveLength(2);

          wrapper.find(Product).forEach(product => {
            expect(product.text()).toContain(SEARCH_TERM);
          });
        });

        describe('and it\'s not matching the case', () => {
          const SEARCH_TERM_CASE_INSENSITIVE = 'sEaRch tItLe';

          beforeEach(() => {
            wrapper.find('.products__search').simulate('change', { target: { value: SEARCH_TERM_CASE_INSENSITIVE } });
          });

          it('only displays the products that match the title', () => {
            expect(wrapper.find(Product)).toHaveLength(2);

            wrapper.find(Product).forEach(product => {
              expect(product.text().toLowerCase()).toContain(SEARCH_TERM_CASE_INSENSITIVE.toLowerCase());
            });
          });

        });
      });

      describe('and the value is part of the description of some products', () => {
        const SEARCH_TERM = 'Search Description';

        beforeEach(() => {
          wrapper.find('.products__search').simulate('change', { target: { value: SEARCH_TERM } });
        });

        it('only displays the products that match the description', () => {
          expect(wrapper.find(Product)).toHaveLength(2);
        });

        describe('and it\'s not matching the case', () => {
          const SEARCH_TERM_CASE_INSENSITIVE = 'sEaRch dEsCriPtiOn';

          beforeEach(() => {
            wrapper.find('.products__search').simulate('change', { target: { value: SEARCH_TERM_CASE_INSENSITIVE } });
          });

          it('only displays the products that match the description', () => {
            expect(wrapper.find(Product)).toHaveLength(2);
          });

        });
      });
    });
  });
});
