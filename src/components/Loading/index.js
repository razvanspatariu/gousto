import React from 'react';
import './index.css';

const Loading = () => <div className="loading"><span className="loading__text">Loading&hellip;</span></div>;

export default Loading;
